# Read Me #

## This project include timeline which: ##

    1. Events are place respectively to the date.
    2. Progressbar is refreshing its width adequately to the date of the month.
    3. String data are loaded from two-dimentional array.
    4. String in popup are dinamicly cuted dependingly to the width of the screen.


## Author information ##

Ernest  Kost

ernest.kost@gmail.com



## Installation and launching ##


### 1. Installing npm packages ###

  In order to launch the app, firstly you need to install the required npm packages. 
  Change your directory to folder with gulfile.js and use the command:
  
  `npm install`
  
  
### 2. Launching the app in production environment ###

  In order to see website in developer environment, use commmand:
  
  `npm start`
  
  
### 3. Creating files for distribution ###

  In order to produce files ready to upload on server, you need to create them by using command:
  
  `npm run build`
  
  Command will delete all of content inside the dist folder and will generate new in the same location minified and concatenated webside ready to be deployed on server.
  
  
###  Used tools and frameworks ###

  
  - [gulp](https://gulpjs.com/)
  - [Font Awesome](http://fontawesome.io/)
  - [Font Open Sans via Google Fonts](https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans)
  - [Handlebars.js](http://handlebarsjs.com/)
  - [SASS](http://sass-lang.com/)
  
  
