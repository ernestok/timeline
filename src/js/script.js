document.addEventListener('DOMContentLoaded', function () {
    handlebarsOn();
    hoverPopup();
    positionCircles();
    stretchBar();
    checkedCicrle();
    textChangePopup();
}, false);


// a two-dimensional array given at the beginning of task
var originTable = [
    ['02.06.2015', 'Lorem ipsum dolor sit amet', 'fa-heart'],
    ['11.06.2015', 'Lorem ipsum dolor sit amet', 'fa-flask'],
    ['15.06.2015', 'Lorem ipsum dolor sit amet', 'fa-gavel'],
    ['22.06.2015', 'Lorem ipsum dolor sit amet', 'fa-graduation-cap'],
    ['30.06.2015', 'Lorem ipsum dolor sit amet', 'fa-trophy']
];


//obtaining day of the year
var dayOfTheYear = function () {
    var calendarDate = originTable.map(function (keyValue) {
        return keyValue[0];
    });
    var splitedDate = [];

    for (var i = 0; i < calendarDate.length; i++) {
        splitedDate[i] = calendarDate[i].split(".");
    }
    var dayDate = splitedDate.map(function (keyValue) {
        return parseInt(keyValue[0], 10);
    });
    return dayDate;
};


// crating array for handlebars.js
var handlebarsArray = function () {
    var allData = [];
    for (var i in originTable) {

        var column = originTable[i];
        allData.push({
            "date": column[0],
            "name": column[1],
            "icon": column[2],
            "day": dayOfTheYear()[i]
        });
    }
    return allData;
};


//handlebars.js function
function handlebarsOn() {

    var quoteInfo = document.getElementById("timeline-template").innerHTML;
    var template = Handlebars.compile(quoteInfo);
    var quoteData = template({
        inputTableData: handlebarsArray()
    });
    document.getElementById('timeline__wrapper').innerHTML += quoteData;
}


//lights the circleEvent and show the popupCloud
function hoverPopup() {
    var circleArea = document.getElementsByClassName('o-circleArea');

    for (var i = 0; i < circleArea.length; i++) {
        circleArea[i].addEventListener("mouseover", function () {
            var circleEventIn = this;
            var popupCloudOn = circleEventIn.parentElement.parentElement.querySelector('.o-popup');
            circleEventIn.classList.add("hover");
            popupCloudOn.classList.add('active');
        });
        circleArea[i].addEventListener("mouseout", function () {
            var circleEventOut = this;
            var popupCloudOff = circleEventOut.parentElement.parentElement.querySelector('.o-popup');
            circleEventOut.classList.remove("hover");
            popupCloudOff.classList.remove('active');
        })
    }
}


//changing colors of already done circle for darker blue
function checkedCicrle() {
    var allCircles = document.getElementsByClassName("o-circleArea");
    var d = new Date();
    var n = d.getDate();

    for (var i = 0; i < allCircles.length; i++) {
        var eachNr = allCircles[i].parentElement.parentElement.getAttribute("data-nr");
        if (eachNr <= n) {
            allCircles[i].parentElement.classList.add('js-static__color');
        }
    }
}


//position of circles on timeline
function positionCircles() {
    var positionCircle = document.getElementsByClassName("o-event__container");

    for (var i = 0; i < positionCircle.length; i++) {
        var whichDay = positionCircle[i].getAttribute("data-nr");
        positionCircle[i].style.left = whichDay * (100 / 30) + "%";
    }
}


//stretching bar adequately to the day in month
function stretchBar() {
    var positionBar = document.querySelector("#js-timeline__line--bottom");
    var d = new Date();
    var n = d.getDate();

    positionBar.style.width = n * (100 / 30) + "%";

}


//appending text accordingly to screen size
function textChangePopup() {
    var middleText = originTable.map(function (keyValue) {
        return keyValue[1];
    });

    var shortenMiddleText = [];

    for (var i = 0; i < middleText.length; i++) {
        shortenMiddleText[i] = middleText[i].substring(0, 22);
    }

    function changeText() {
        clearText();

        var innerString = document.getElementsByClassName('o-popup__name');
        for (var i = 0; innerString.length > i; i++) {
            var singleText = innerString[i];

            if (window.innerWidth >= 480) {

                var txt2 = document.createTextNode(shortenMiddleText[i]);
                singleText.appendChild(txt2);
            }
            else {
                if (window.innerWidth < 480) {

                    var txt = document.createTextNode(middleText[i]);
                    singleText.appendChild(txt);
                }
            }
        }
    }

    window.onresize = throttle(function () {
        changeText();
    }, 100);
    changeText();
}


//remove text in popups
function clearText() {

    var el = document.getElementsByClassName('o-popup__name');
    for (var i = 0; i < el.length; i++) {
        var child = el[i].firstChild;
        if (child.nodeType == 3 || undefined) {
            child.parentNode.removeChild(child);
        }
    }

}


//throttle function
function throttle(fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshhold) {
            // hold on to it
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshhold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}