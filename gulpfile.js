var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');
var htmlReplace = require('gulp-html-replace');
var htmlMin = require('gulp-htmlmin');
var del = require('del');
var sequence = require('run-sequence');
var strip = require('gulp-strip-comments');
var order = require("gulp-order");


var config = {
    dist: 'dist/',
    src: 'src/',
    cssin: 'src/css/**/*.css',
    jsin: 'src/js/**/*.js',
    imgin: 'src/img/**/*.{jpg,jpeg,png,gif,ico}',
    htmlin: 'src/*.html',
    scssin: 'src/scss/**/*.scss',
    scssinmain: 'src/scss/*.scss',
    fontsin: 'src/css/fonts/*',
    icoin: 'src/favicon.ico',

    cssout: 'dist/css/',
    jsout: 'dist/js/',
    imgout: 'dist/img/',
    htmlout: 'dist/',
    scssout: 'src/css/',
    cssoutname: 'style.css',
    jsoutname: 'script.js',
    cssreplaceout: 'css/style.css',
    jsreplaceout: 'js/script.js',
    fontsout: 'dist/fonts/'
};

gulp.task('reload', function () {
    browserSync.reload();
});

gulp.task('serve', ['sass'], function () {

    browserSync({
        server: config.src
    });

    gulp.watch([config.htmlin, config.jsin], ['reload']);
    gulp.watch(config.scssin, ['sass']);
});


gulp.task('sass', function () {
    return gulp.src(config.scssinmain)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 1 version', 'Firefox > 42', 'Chrome > 46', 'ie>=11']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.scssout))
        .pipe(browserSync.stream());
});

gulp.task('css', function () {
    return gulp.src(config.cssin)
        .pipe(order([
            "vendors/*.css",
            "/*.css"
        ]))
        .pipe(concat(config.cssoutname))
        .pipe(cleanCSS())
        .pipe(gulp.dest(config.cssout));
});

gulp.task('js', function () {
    return gulp.src(config.jsin)
        .pipe(concat(config.jsoutname))
        .pipe(uglify())
        .pipe(gulp.dest(config.jsout));
});

gulp.task('img', function () {
    return gulp.src(config.imgin)
        .pipe(changed(config.imgout))
        .pipe(imagemin())
        .pipe(gulp.dest(config.imgout))
});

// /* pipe(strip()) remove comments gulp-strip-comments*/
gulp.task('html', function () {
    return gulp.src(config.htmlin)
        .pipe(htmlReplace({
            'css': config.cssreplaceout,
            'js': config.jsreplaceout
        }))
        .pipe(strip())
        .pipe(htmlMin({
            sortAttributes: true,
            sorClassName: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(config.htmlout))
});

// move fonts folder to dist
gulp.task('fonts', function () {
    return gulp.src(config.fontsin)
        .pipe(gulp.dest(config.fontsout))
});

gulp.task('ico', function () {
    return gulp.src(config.icoin)
        .pipe(gulp.dest(config.dist))
});

gulp.task('clean', function () {
    return del([config.dist]);
});

gulp.task('build', function () {
    sequence('clean', 'sass', 'css', ['html', 'js', 'img', 'fonts', 'ico']);
});

gulp.task('default', ['serve'], function () {
    console.log("domyslne zadanie");
});
